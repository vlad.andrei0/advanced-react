import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { ALL_ITEMS_QUERY } from './Items'

const DELETE_ITEM_MUTATION = gql`
    mutation DELETE_ITEM_MUTATION($id: ID!) {
        deleteItem(id: $id) {
            id
        }
    }
`
// UPDATE CACHE LOGIC 
class DeleteItem extends Component {
    update = (cache, payload) => {
        console.log('update')
        // read cache
        const data = cache.readQuery({ query: ALL_ITEMS_QUERY })
        // remove deleted item from the cache list
        data.items = data.items.filter(item => item.id !== payload.data.deleteItem.id);
        // update the cache with the new data object
        cache.writeQuery({ query: ALL_ITEMS_QUERY, data: data })
    }

    render() {
        return (
            <Mutation update={this.update}
                mutation={DELETE_ITEM_MUTATION}
                variables={{
                    id: this.props.id
                }}>
                {(deleteItem, { error }) => {
                    return (
                        <button onClick={() => {
                            if (confirm('Are u sure u want to delete tis item?')) {
                                deleteItem().catch(err => {
                                    alert(err.message);
                                })
                            }
                        }}>{this.props.children}</button>
                    )
                }
                }
            </Mutation>
        );
    }
}

export default DeleteItem;