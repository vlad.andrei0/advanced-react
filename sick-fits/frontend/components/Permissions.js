import { Query, Mutation } from 'react-apollo';
import Error from './ErrorMessage';
import gql from 'graphql-tag';
import PropTypes from 'prop-types';


import Table from '../components/styles/Table'
import SickButton from '../components/styles/SickButton';

const possiblePermissions = [
    'ADMIN',
    'USER',
    'ITEMCREATE',
    'ITEMUPDATE',
    'ITEMDELETE',
    'PERMISSIONUPDATE'
]

const ALL_USERS_QUERY = gql`
    query ALL_USERS_QUERY {
        users {
            id
            name
            email
            permissions
        }
    }
`
const UPDATE_PERMISSIONS_MUTATION = gql`
    mutation updatePermissions($permissions: [Permission], $userId: ID!) {
        updatePermissions(permissions: $permissions, userId: $userId) {
            id
            permissions
            name
            email
        }
    }
`

const Permissions = (props) => {

    return (
        <Query query={ALL_USERS_QUERY}>
            {({ data, loading, error }) => {
                console.log('data', data)
                return (
                    <div>
                        <Error error={error} />
                        <div>
                            <Table>
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        {possiblePermissions.map(permission =>
                                            <th key={permission}>{permission}</th>)}
                                        <th>&darr;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {data.users.map(user => <User key={user.id} user={user} />)}
                                </tbody>
                            </Table>
                        </div>
                    </div>
                )
            }}
        </Query>
    )
}

const User = ({ user }) => {
    const [permissionsState, setPermissionsState] = React.useState({
        permissions: user.permissions
    })
    console.log(permissionsState, user.id)

    const handlePermissionChange = (e) => {
        const checkbox = e.target;
        // copy of permissions state to avoid mutation
        let updatedPermissions = [...permissionsState.permissions];
        if (checkbox.checked) {
            updatedPermissions.push(checkbox.value);
        } else {
            updatedPermissions = updatedPermissions.filter(permission => permission !== checkbox.value)
        }
        setPermissionsState({ permissions: updatedPermissions })
        console.log(updatedPermissions)
    }
    return (
        <Mutation mutation={UPDATE_PERMISSIONS_MUTATION}
            variables={{
                permissions: permissionsState.permissions,
                userId: user.id,
            }}>
            {(updatePermissions, { loading, error }) => {
                return (
                    <>
                        {error && <tr><Error error={error} /></tr>}
                        <tr>
                            <td>{user.name}</td>
                            <td>{user.email}</td>
                            {possiblePermissions.map(permission => (
                                <td key={permission}>
                                    <label htmlFor={`${user.id}-permissions-${permission}`}>
                                        <input
                                            id={`${user.id}-permissions-${permission}`}
                                            type="checkbox"
                                            checked={permissionsState.permissions.includes(permission)}
                                            value={permission}
                                            onChange={handlePermissionChange}
                                        />
                                    </label>
                                </td>
                            ))}
                            <td>
                                <SickButton
                                    type="button"
                                    disabled={loading}
                                    onClick={updatePermissions}>
                                    Update
                    </SickButton>
                            </td>
                        </tr>
                    </>
                )
            }}
        </Mutation>
    )
}

User.propTypes = {
    user: PropTypes.shape({
        name: PropTypes.string,
        email: PropTypes.string,
        id: PropTypes.string,
        permissions: PropTypes.array,
    }).isRequired
}
export default Permissions;

