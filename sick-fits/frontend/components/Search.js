import React, { useState } from 'react';
import Downshift, { resetIdCounter } from 'downshift';
import Router from 'next/router';
import { ApolloConsumer } from 'react-apollo';
import gql from 'graphql-tag';
import debounce from 'lodash.debounce';
import { DropDown, DropDownItem, SearchStyles } from '../components/styles/DropDown';


const SEARCH_ITEMS_QUERY = gql`
    query SEARCH_ITEMS_QUERY($searchTerm: String!) {
        items(where: {OR: 
            [{name_contains: $searchTerm},
            {description_contains: $searchTerm},]
        }) {
            id
            image
            name
        }
    }
`

const Search = () => {
    const [items, setItems] = useState({ items: [], loading: false });
    // console.log('items', items)

    const onChange = debounce(async (e, client) => {
        // console.log('searching')
        setItems({ ...items, loading: true })
        const res = await client.query({
            query: SEARCH_ITEMS_QUERY,
            variables: { searchTerm: e.target.value },
        })
        setItems({
            items: res.data.items,
            loading: false
        })
    }, 450)
    const routeToItem = (item) => {
        Router.push({
            pathname: '/item',
            query: {
                id: item.id
            }
        })
    }
    resetIdCounter();
    return (
        <SearchStyles>
            <Downshift onChange={routeToItem} itemToString={item => (item === null ? '' : item.name)}>
                {({ getInputProps, getItemProps, isOpen, inputValue, highlightedIndex }) => (
                    <div>
                        <ApolloConsumer>
                            {(client) => {
                                return (
                                    <input {...getInputProps(
                                        {
                                            type: 'search',
                                            placeholder: 'Search for an item',
                                            id: 'search',
                                            className: items.loading ? 'loading' : '',
                                            onChange: e => {
                                                e.persist(),
                                                    onChange(e, client)
                                            }
                                        }
                                    )}
                                    />
                                )
                            }}
                        </ApolloConsumer>
                        {isOpen && (
                            <DropDown>
                                {items.items.map((item, index) => {
                                    return (
                                        <DropDownItem
                                            {...getItemProps({ item })}
                                            key={item.id}
                                            highlighted={index === highlightedIndex}>
                                            <img width="50" src={item.image} alt={item.name} />
                                            {item.name}
                                        </DropDownItem>
                                    )
                                })}
                                {(!items.items.length && !items.loading) &&
                                    <DropDownItem>Nothing found {inputValue}</DropDownItem>}
                            </DropDown>
                        )}
                    </div>
                )}
            </Downshift>

        </SearchStyles>
    )
}
export default Search;
