import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components'
import { Query } from 'react-apollo';
import { formatDistance } from 'date-fns';
import Link from 'next/link';
import gql from 'graphql-tag';
import formatMoney from '../lib/formatMoney';
import Error from './ErrorMessage';
import OrderItemStyles from './styles/OrderItemStyles';

const ORDERS_QUERY = gql`
    query ORDERS_QUERY {
        orders(orderBy: createdAt_DESC){
            id
            total
            createdAt
            items {
                id
                name
                price
                description
                quantity
                image
            }
        }
    }
`
const OrderUl = styled.ul`
    display: grid;
    grid-gap: 4rem;
    grid-template-columns: repeat(auto-fit, minmax(40%, 1fr));

`

const OrderList = () => {
    return (
        <Query query={ORDERS_QUERY}>
            {({ data: { orders }, error, loading }) => {
                console.log('orders', orders)
                if (loading) return (<p>Loading...</p>);
                if (error) return (<Error error={error} />);
                return (
                    <div>
                        <h2>You have {orders.length} orders</h2>
                        <OrderUl>
                            {orders.map(order => {
                                return (
                                    <OrderItemStyles key={order.id}>
                                        <Link href={{
                                            pathname: '/order',
                                            query: { id: order.id }
                                        }}>
                                            <a>
                                                <div className="order-meta">
                                                    <p>{order.items.reduce((a, b) => a + b.quantity, 0)} Items</p>
                                                    <p>{order.items.length} Products</p>
                                                    <p>{formatMoney(order.total)}</p>
                                                </div>
                                                <div className="images">
                                                    {order.items.map(item => (
                                                        <img src={item.image} alt={item.name} />
                                                    ))}
                                                </div>
                                            </a>
                                        </Link>
                                    </OrderItemStyles>
                                )
                            })}
                        </OrderUl>
                    </div>
                )
            }}
        </Query>

    )
}

export default OrderList;
