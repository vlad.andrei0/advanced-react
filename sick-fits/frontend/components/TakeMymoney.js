import React from 'react';
import StripeCheckout from 'react-stripe-checkout';
import { Mutation } from 'react-apollo';
import Router from 'next/router';
import NProgress from 'nprogress';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';
import calcTotalPrice from '../lib/calcTotalPrice';
import Error from './ErrorMessage';
import User, { CURRENT_USER_QUERY } from './User';


const CREATE_ORDER_MUTATION = gql`
    mutation CREATE_ORDER_MUTATION($token: String!) {
        createOrder(token: $token) {
            id
            charge
            total
            items {
                id
                name
            }
        }
    }
`

const TakeMymoney = (props) => {
    const totalItems = (cart) => {
        return cart.reduce((tally, cartItem) => tally + cartItem.quantity, 0)
    }

    const onToken = async (res, createOrder) => {
        NProgress.start();
        const order = await createOrder({
            variables: {
                token: res.id
            }
        }).catch(err => {
            alert(err.message)
        });
        Router.push({
            pathname: '/order',
            query: { id: order.data.createOrder.id }
        })
        console.log('order', order)
    }
    return (
        <User>
            {({ data: { me } }) => {
                return (
                    <Mutation
                        mutation={CREATE_ORDER_MUTATION}
                        refetchQueries={[{ query: CURRENT_USER_QUERY }]}>
                        {(createOrder) => (

                            <StripeCheckout
                                amount={calcTotalPrice(me.cart)}
                                name="Sick Fits"
                                description={`Order of ${totalItems(me.cart)} items!`}
                                image={me.cart.length && me.cart[0].item && me.cart[0].item.image}
                                stripeKey="pk_test_51HEW7xKYWE4NpGqgrKTUO6U5tLXbnbL9ULLhSidOMZqGUyiPgmMLd0Uasac9vRcEZzKxRxMl82YJRDrrjTnZEmCs00wHt6BMWG"
                                currency="USD"
                                email={me.email}
                                token={res => onToken(res, createOrder)}
                            >
                                {props.children}</StripeCheckout>
                        )}
                    </Mutation>
                )
            }}
        </User>
    )
}
export default TakeMymoney;
