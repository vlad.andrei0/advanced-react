import OrderList from '../components/OrderList';
import PleaseSignIn from '../components/PleaseSignIn';

const OrdersPage = ({ query }) => {
    console.log(query);
    return (
        <div>
            <PleaseSignIn>
                <OrderList />
            </PleaseSignIn>
        </div>
    )
}

export default OrdersPage;