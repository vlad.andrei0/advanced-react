const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { randomBytes } = require('crypto');
const { promisify } = require('util');
const { transport, makeANiceEmail } = require('../mail.js')
const { hasPermission } = require('../utils');
const stripe = require('../stripe')


const Mutations = {
    async createItem(parent, args, ctx, info) {
        if (!ctx.request.userId) {
            throw new Error('You must be logged in to do that')
        }

        const Item = await ctx.db.mutation.createItem({
            data: {
                // provide relationship in prisma between data types
                user: {
                    connect: {
                        id: ctx.request.userId,
                    }
                },
                ...args
            }
        }, info);
        return Item;
    },
    updateItem(parent, args, ctx, info) {
        const updates = { ...args };
        delete updates.id;

        return ctx.db.mutation.updateItem({
            data: updates,
            where: {
                id: args.id
            }
        }, info);
    },
    async deleteItem(parent, args, ctx, info) {
        const where = { id: args.id };
        // // 1. Find the item
        const item = await ctx.db.query.item({ where }, `{id name user {id}}`);
        const ownsItem = item.user.id === ctx.request.userId;
        const hasPermissions = ctx.request.user.permissions.some(permission => ['ADMIN', 'ITEMDELETE'].includes(permission));
        if (!ownsItem && !hasPermissions) {
            throw new Error("You don't have permissions to do that")
        }
        // // 3. Delete the item
        return ctx.db.mutation.deleteItem({ where }, info);
    },
    async signup(parent, args, ctx, info) {
        args.email = args.email.toLowerCase();
        const password = await bcrypt.hash(args.password, 10);
        const user = await ctx.db.mutation.createUser(
            {
                data: {
                    ...args,
                    password,
                    permissions: { set: ['USER'] },
                }
            }, info
        );
        // create the JWT token for them
        const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);
        // We set the jwt as a cookie on the  response
        ctx.response.cookie('token', token, {
            httpOnly: true,
            maxAge: 1000 * 60 * 60 * 24 * 365,
        })
        // return user to the browser
        return user;
    },
    async signin(parent, { email, password }, ctx, info) {
        const user = await ctx.db.query.user({ where: { email: email } })
        if (!user) {
            throw new Error(`No such user found for email ${email}`)
        }
        const valid = await bcrypt.compare(password, user.password)
        if (!valid) {
            throw new Error(`Invalid password!`);
        }
        const token = jwt.sign({ userId: user.id }, process.env.APP_SECRET);
        // We set the jwt as a cookie on the  response
        ctx.response.cookie('token', token, {
            httpOnly: true,
            maxAge: 1000 * 60 * 60 * 24 * 365,
        })
        return user;
    },
    signout(parent, args, ctx, info) {
        ctx.response.clearCookie('token');
        return { message: 'Goodbye' };
    },
    async requestReset(parent, args, ctx, info) {
        // check for real user 
        const user = await ctx.db.query.user({ where: { email: args.email } });
        if (!user) {
            throw new Error(`No such user found for email ${args.email}`);

        }
        // set a reset token on that user
        const randomBytesPromiseified = promisify(randomBytes);
        const resetToken = (await randomBytesPromiseified(20)).toString('hex');
        const resetTokenExpiry = Date.now() + 3600000;
        const res = await ctx.db.mutation.updateUser({
            where: { email: args.email },
            data: { resetToken: resetToken, resetTokenExpiry: resetTokenExpiry }
        })
        // Email the reset token
        const mailRes = await transport.sendMail({
            from: 'wes@wesbos.com',
            to: user.email,
            subject: 'Your Password reset token is here!',
            html: makeANiceEmail(`Your password reset token is here
                \n\n
                <a href="${process.env.FRONTEND_URL}/reset?resetToken=${resetToken}">
                Click here to reset
                </a>
                `)
        })
        // return the mesage
        return { message: 'Thanks' }
    },
    async resetPassword(parent, args, ctx, info) {
        if (args.password !== args.confirmPassword) {
            throw new Error('Yo passwords dont match')
        };
        const [user] = await ctx.db.query.users({
            where: {
                resetToken: args.resetToken,
                resetTokenExpiry_gte: Date.now() - 3600000
            }
        })
        if (!user) {
            throw new Error('This token is expired or invalid');
        }
        const password = await bcrypt.hash(args.password, 10);
        const updatedUser = await ctx.db.mutation.updateUser({
            where: { email: user.email },
            data: {
                password,
                resetToken: null,
                resetTokenExpiry: null
            }
        })
        const token = jwt.sign({ userId: updatedUser.id }, process.env.APP_SECRET);
        ctx.response.cookie('token', token, {
            httpOnly: true,
            maxAge: 1000 * 60 * 60 * 24 * 365,
        })
        return updatedUser;

    },
    async updatePermissions(parent, args, ctx, info) {
        if (!ctx.request.userId) {
            throw new Error('You must be logged in!')
        }
        const currentUser = await ctx.db.query.user(
            {
                where: {
                    id: ctx.request.userId,
                }
            }, info
        )
        hasPermission(currentUser, ['ADMIN', 'PERMISSIONUPDATE']);
        return ctx.db.mutation.updateUser({
            data: {
                permissions: {
                    set: args.permissions,
                }
            },
            where: {
                id: args.userId,
            },
        }, info)
    },
    async addToCart(parent, args, ctx, info) {
        // console.log('ctx', ctx)
        // check if the user is signed in
        const userId = ctx.request.userId;
        if (!userId) {
            throw new Error("You must be signed in")
        }
        // Query the user current cart
        const [existingCartItem] = await ctx.db.query.cartItems({
            where: {
                user: { id: userId },
                item: { id: args.id },
            }
        })
        // Check if that item is already in the cart and increment by 1 if it is
        if (existingCartItem) {
            console.log("This item already exists")
            return ctx.db.mutation.updateCartItem({
                where: { id: existingCartItem.id },
                data: { quantity: existingCartItem.quantity + 1 }
            }, info)
        }
        // if its not, create fresh CartItem for that user
        return ctx.db.mutation.createCartItem({
            data: {
                user: {
                    connect: { id: userId }
                },
                item: {
                    connect: { id: args.id }
                },
            },
        }, info)
    },

    async removeFromCart(parent, args, ctx, info) {
        const cartItem = await ctx.db.query.cartItem(
            {
                where: {
                    id: args.id
                },
            }, `{id, user {id}}`
        );
        // console.log('cartItem', cartItem)
        if (!cartItem) throw new Error('No Cart Item found')

        if (cartItem.user.id !== ctx.request.userId) {
            throw new Error('NOPE')
        }
        return ctx.db.mutation.deleteCartItem({
            where: { id: args.id },
        }, info)

    },
    async createOrder(parent, args, ctx, info) {
        const { userId } = ctx.request;
        if (!userId) throw new Error('You must be signed in to complete de order');

        const user = await ctx.db.query.user(
            { where: { id: userId } },
            `{
                id
                name
                email
                cart {
                    id
                    quantity
                    item {name price id description image largeImage}
                }
            }`
        );
        // recalculate total price
        const amount = user.cart.reduce((tally, cartItem) => tally + cartItem.item.price * cartItem.quantity, 0);
        console.log('going to charge', amount)
        // create stripe charge
        const charge = await stripe.charges.create({
            amount,
            currency: 'USD',
            source: args.token,
        })
        // convert cartitem to Orderitems
        const orderItems = user.cart.map(cartItem => {
            const orderItem = {
                ...cartItem.item,
                quantity: cartItem.quantity,
                user: { connect: { id: userId } }
            };
            delete orderItem.id;
            return orderItem;
        });
        const order = await ctx.db.mutation.createOrder({
            data: {
                total: charge.amount,
                charge: charge.id,
                items: { create: orderItems },
                user: { connect: { id: userId } },
            },
        });
        // clean up the users cart, delete cartItems
        const cartItemsIds = user.cart.map(cartItem => cartItem.id);
        await ctx.db.mutation.deleteManyCartItems({
            where: {
                id_in: cartItemsIds
            }
        });
        // return order to client
        return order;


    }

};

module.exports = Mutations;
