const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');

require('dotenv').config({ path: 'variables.env' });
const createServer = require('./createServer');
const db = require('./db');

const server = createServer();

server.express.use(cookieParser());

// TODO Use express middlware to populate current user
server.express.use((req, res, next) => {
    const { token } = req.cookies;
    if (token) {
        const { userId } = jwt.verify(token, process.env.APP_SECRET);
        //put the userId in the request for future requests to access
        req.userId = userId;
    }
    next();
})

server.express.use(async (req, res, next) => {
    // check if the user is logged in
    if (!req.userId) return next();
    const user = await db.query.user(
        {
            where: { id: req.userId },
        },
        '{id, permissions, email, name}'
    )
    req.user = user;
    next();
})

server.start(
    {
        cors: {
            credentials: true,
            origin: process.env.FRONTEND_URL,
        },
    },
    details => {
        console.log(`Server is now running on port http:/localhost:${details.port}`);
    }
)